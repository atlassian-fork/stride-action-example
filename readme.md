# README #

## What is this? ##

This is an example app for Atlassian Stride.
This app shows how you can use actions to open a modal window in stride from various places in the applicaiton.

## How do I get set up? ##

### Prerequisites ###

* Make sure you have node 8.9.x or above on your machine
* Make sure you have the latest version of ngrok installed
* Sign up for an account on [https://developer.atlassian.com](https://developer.atlassian.com)
* [Create a new app](https://developer.atlassian.com/apps/create). Let's call it the 'Stride Actions Example App'.
* Once your app is created you need to:
  * Go into the 'App Features' tab and enabled the bot account for your app.
  * Go into the 'Enabled APIs' tab and copy the Stride API Client Id and Client Secret, we'll need this later.

### Running the app ###

* Clone this repository to your local machine. ```git clone git@bitbucket.org:atlassian/stride-action-example.git```
* Open a terminal window and go into your local repository to run ```npm install```.
* Once that command is finished you'll need to run ```PORT=3000 CLIENT_ID={clientId} CLIENT_SECRET={clientSecret} npm run dev ``` or you can also store the PORT, CLIENT_ID, and CLIENT_SECRET in .env file
* Now open a second terminal window and run ```ngrok http 3000```.
* Copy the https url from your Ngrok output, this is where your descriptor lives.
* Go back to your app page on [https://developer.atlassian.com/apps](https://developer.atlassian.com/apps)
* Go into the 'Install' tab and paste your ngrok url in the Descriptor url field.
* Click 'Refresh'
* You app is now live and ready for use.

### Installing the app ###

* Copy the installation url from your 'Install' tab in your app page.
* Go into a Stride room and click on the 'Apps' glance on the right side of your screen, this will open up a sidebar.
* Click on the + icon at the top of the sidebar, this will open internal marketplace and show the installed apps for your Stride room.
* Click on the 'Add custom app' link at the top of the page, this will open up a dialog where you can paster your installation URL. This will load your app's information into the dialog.
* Click 'Agree' to install the app.

### Seeing it in action ###

* **App Mention:** Mention the app in your room and it will send a message with an **application card** and a "view dialog" link. Clicking this will open the dialog modal 
* **Message Link:** Mention the word "action" and a **message link** that will open a dialog will appear. Clicking this will open the dialog modal
* **Input Action:** Click on the **"meatball" •••**   menu on the right side of the message input and you will see an option for "Open Dialog". Clicking this will open the dialog modal.
* **Message Action** On any submitted message text you can click on the **"meatball" •••** menu on the right side of the message text and you will see and option for "Send to Dialog"
* **Sidebar Action:** Click on the right hand icon call a glance of the blue thunderbolt and the sidebar should slide open. Click on the 2 buttons to open the dialog window and see 2 ways to open the window

### How the code works ###
This example focusses on using all the different action types in stride.
In this example we cover
- Input Action
- Action Targets
- Message Action
  - Mark Action
- Actions as links in messages
- Actions as buttons in Application Cards
- Actions from the Javascrip API

The bulk of our example code can be found in **./routes/index.js.**

When you mention the app in a conversation, this will trigger the **/bot-mention** endpoint (describe in our app descriptor).

When you mention the word actions in a conversation the **chat:bot:messages** in the descriptor will trigger the **/action-message** endpoint and send a message with a link to open a modal dialog window.

This bot-mention endpoint gets captured in our **./routes/index.js** file and calls our markdown module to send a message using the **sendMarkdownMessage** function.

**Messages** **action-card.js** and **actions-messages.js** are raw JSON objects that get sent as a payload to stride from the **./lib/send-message.js** file. **Note** we did not use the **[ADF format](https://developer.atlassian.com/cloud/stride/apis/document/libs/)** so that you can understand how the raw JSON is formated.

The **./lib/jwt.js** helps to create a jwt token for authentication when sending messages to stride by using your  **CLIENT SECRET**

##Reference Documentation##
https://developer.atlassian.com/cloud/stride/learning/actions/
### Input Action ###
https://developer.atlassian.com/cloud/stride/apis/modules/chat/inputAction/
### Action targets ###
https://developer.atlassian.com/cloud/stride/apis/modules/chat/actionTarget/
### Message action ###
https://developer.atlassian.com/cloud/stride/apis/modules/chat/messageAction/

#### Mark Action ####
https://developer.atlassian.com/cloud/stride/apis/document/marks/action/

### Actions as links in messages ###
https://developer.atlassian.com/cloud/stride/apis/document/marks/link/

### Actions as buttons in Application Cards ###
https://developer.atlassian.com/cloud/stride/apis/document/nodes/applicationCard/

### Javascript API ###
https://developer.atlassian.com/cloud/stride/apis/jsapi/action/openTarget/

